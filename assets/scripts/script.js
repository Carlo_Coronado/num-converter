// Arrays

// let students = ['Darwin', 'Rancel', 'Yong', 'Kaka'];
// let randomThings = [23, "Albert", true];
// console.log(randomThings);

// console.log(students[0]);

// .length
// console.log(students.length);

// let password = "iloveyou";
// console.log(password.length);


// add value to an array 
// .push()

let students = ['Darwin', 'Rancel', 'Yong', 'Kaka'];
// students.push('Krystel')
console.log(students);

// 	To remove the last value in an array and return the mnew array
// .pop

// To remove an element based on its index;
// .splice(index, # of element we want to remove)

// Iteration Method
// .forEach

// students.forEach(function(student, index){
// 	console.log(student + " Pogi");
// });

// students.forEach(function(student, index){
// 	console.log("Contestant #" + index + ": " +student + " Pogi");
// });

// students.forEach(function(student, index){
// 	console.log("Contestant #" + (index+1) + ": " +student + " Pogi");
// });

// students.forEach(function(student, index){
// 	if (index > 1){
// 	console.log("Contestant #" + index + ": " + student + " Pogi");
// 	}else{
// 		console.log("Thank you for joining" + student);
// 	}
// });

// students.forEach(function(student, index){
// 	if (index === 0 || index ===3){
// 	console.log("Contestant #" + (index+1) + ": " + student + " Pogi");
// 	}else{
// 		console.log("Thank you for joining" + student);
// 	}
// });



