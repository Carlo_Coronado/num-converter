// Steps

let singleDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"]

let tens = ["", "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eigthy", "ninety"]

let teens = ["", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]

let hundred = ["hundred"]

// let hundred = ["", "one hundred", "two hundred" "three hundred", "four hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred eight", "one hundred nine", "one hundred ten"]

function numConverter(inputNo)
{
	if(inputNo < 10 && inputNo >= 0){
		return singleDigit[inputNo]
	}

	if(inputNo < 99 && inputNo % 10 === 0){
		return tens[inputNo/10]
	}
	if(inputNo<20 && inputNo>10){
		return teens[inputNo-10]
	}
	if(inputNo>20 && inputNo < 100){
		let singleValue = inputNo % 10;
		let tensValue = (inputNo - singleValue)/10;
		return tens[tensValue] + " " + singleDigit[singleValue]
	}
	// if(inputNo%100 > 10 && inputNo%100 < 10){
	// 	let teensValue = inputNo % 100;
	// 	let hundredValue = (inputNo-teensValue/100);
	// 	return singleDigit[hundredValue] + "hundred" + singleDigit[inputNo]
	// }

	// if(inputNo%100 > 10 && inputNo%100 < 10){
	// 	let teensValue = inputNo % 100;
	// 	let hundredValue = (inputNo-teensValue/100);
	// 	return singleDigit[hundredValue] + " hundred" + teens[teensValue]
	// }

	if(inputNo <999 && inputNo % 10 === 0) {
		let tenshundred = (inputNo&100)/10;
		let hundred = (inputNo-(tenshundred*10))/100;
		return singleDigit(hundred) + "hundred" + tens(tenshundred)
	}
}